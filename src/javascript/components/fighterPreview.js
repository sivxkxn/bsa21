import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)

  if (fighter) {
    let info = createFighterInfo(fighter);
    let photo = createFighterImage(fighter);
    fighterElement.prepend(photo, info);
    return fighterElement;

  }
  else if (fighter === false) {
    const error = createLoadErrorElement('Failed to load');
    fighterElement.append(error);
  }
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}


// some of my code for fighter info
function createFighterInfo(fighter) {
  try {
    let fighterInfo = createElement({
      tagName: 'div',
      className: 'fighter-preview___info'
    });
    let name = createElement({
      tagName: 'h3',
      className: 'fighter-preview__info-name',
    });
    name.innerHTML = `${fighter.name}`;
    let health = createElement({ tagName: 'p', className: 'fighter-preview___info-details' });
    health.innerHTML = `&#128154; Health: ${fighter.health}`;
    let attack = createElement({ tagName: 'p', className: 'fighter-preview___info-details' });
    attack.innerHTML = `&#128165; Attack: ${fighter.attack}`;
    let defense = createElement({ tagName: 'p', className: 'fighter-preview___info-details' });
    defense.innerHTML = `&#128737; Defense: ${fighter.defense}`;
    fighterInfo.prepend(name, health, attack, defense);
    console.log(fighterInfo);
    return fighterInfo;
  }
  catch (e) {
    throw e;
  }
}
function createLoadErrorElement(textError) {
  const element = createElement({
    tagName: 'div',
    className: 'fighter-preview___info fighter-preview___load-error'
  });
  element.innerText = textError;
  return element;
}
// let a = createElement({ tagName: 'span', className: 'fighter-preview___info-details' });
// a.innerHTML = "hi";
// let b = createElement({ tagName: 'div', className: 'fighter-preview___info-details' });
// b.prepend(a);
// console.log(b)