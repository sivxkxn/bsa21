import { callApi } from '../helpers/apiHelper';

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
    // console.log(id);
    // todo: implement this method
    try {
      const endpoint = `details/fighter/${id}.json`;
      const res = await callApi(endpoint, 'GET');
      console.log(res);
      return res;
    }
    catch (e) {
      throw e;
    }

  }
}

export const fighterService = new FighterService();
